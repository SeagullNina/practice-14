import Game from "/Game.js"
import Card from "/Card.js"
import SpeedRate from "/SpeedRate.js"

// Отвечает является ли карта уткой.
function isDuck(card) {
    return card && card.quacks && card.swims;
}

// Отвечает является ли карта собакой.
function isDog(card) {
    return card instanceof Dog;
}

function isLad(card){
    return card instanceof Lad;
}

// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
    if (isDuck(card) && isDog(card)) {
        return 'Утка-Собака';
    }
    if (isDuck(card)) {
        return 'Утка';
    }
    if (isDog(card)) {
        return 'Собака';
    }
    if (isLad(card)){
        return 'Чем их больше, тем они сильнее';    }

    return 'Существо';
}

class Creature extends Card{
    constructor(){
        super();
    }
    getDescriptions()
    {
        let result = [];
        result.push(this.getInheritanceDescription);
        result.push(getCreatureDescription(this));
        return result;
    }
}

// Основа для утки.
class Duck extends Creature{
    constructor(name, maxPower, image){
        super();
    }

    quacks() { 
        console.log('quack') 
    }
    swims() { 
        console.log('float: both;') 
    }
}

// Основа для собаки.
class Dog extends Creature{
    constructor(name, maxPower, image){
    super();
    }

    swims() { 
        console.log('float: none;') 
    };
}

class Lad extends Dog{
    constructor(){
        super();
    }
    static getBonus(){
        return this.inGameCount()*(this.inGameCount() + 1)/2;
    }
    static getInGameCount() 
    { 
        return this.inGameCount || 0; 
    }
    static setInGameCount(value) 
    { 
        this.inGameCount = value; 
    }
    doAfterComingIntoPlay(){
        this.inGameCount++;
    }
    doBeforeRemoving(){
        this.inGameCount--;
    }
    modifyDealedDamageToCreature(value)
    {
        super.modifyDealedDamageToCreature(value + Lad.getBonus(), toCard, gameContext, continuation);
    }

    modifyTakenDamage(value, fromCard, gameContext, continuation){
        super.modifyTakenDamage(value - Lad.getBonus(), fromCard, gameContext, continuation);
    }

    getDescriptions = () => {
        if (Lad.prototype.hasOwnProperty('modifyDealedDamageToCreature') || Lad.prototype.hasOwnProperty('modifyTakenDamage')) {
            return ['Чем их больше, тем они сильнее']
        }
        return [getCreatureDescription(this)]
    }

}


// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
    new Duck('Мирная утка', 2),
    new Duck('Мирная утка', 2),
    new Duck('Мирная утка', 2)
];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [
    new Dog('Пес-бандит', 3),
];


// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
SpeedRate.set(1);

// Запуск игры.
game.play(false, (winner) => {
    alert('Победил ' + winner.name);
});
